<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/png" href="/mewpipe/vue/css/img/favicon.ico" />
    <link rel="stylesheet" href="/mewpipe/vue/css/style.css" />

    <link rel="stylesheet" href="/mewpipe/vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="/mewpipe/vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />


    <title>Completing your profile</title>
</head>

<div class="container-fluid">

    <body>
    <div class="row-fluid">
        <div class="span3 offset5">
            <p>You can use your informations from other providers for your profile and log on later with those services</p><br/>
            <a href="<?php echo $openid_url; ?>">Use informations from Google</a><br/><br/>
            <a href="/mewpipe/profil">I don't want to use other services</a>
        </div>
    </div>
    </body>

</div>


</html>