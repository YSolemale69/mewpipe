<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/png" href="vue/css/img/favicon.ico" />
        <link rel="stylesheet" href="../vue/css/style.css" />
        
        <link rel="stylesheet" href="../vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="../vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />



    <title>Edit video</title>
</head>

<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span2">
            <?php
                //include_once 'vue/template/sidebar_gauche.php';
            ?>
        </div> -->

        <div class="span8 offset2">
            <?php
            include_once 'vue/template/header.php';
            ?>

            <body>
            <div class="row-fluid">
                <div class="span12">
                    <form method="post" action="" id="formVideo" enctype="multipart/form-data">
                        <fieldset>
                            <legend>Editer une vidéo</legend>

                            <p><input type="text" class="input-xlarge" name="titre"  value="<?php echo $get_video_by_name['titre']?>" required><br></p>
                            <p><textarea class="input-xlarge" name="desc"><?php echo $get_video_by_name['description']?></textarea><br></p>
                            <p><input type="radio" name="conf" value="0" <?php echo $get_video_by_name['confidentialite']==0?"checked":"" ?>>Public </input>
                            <input type="radio" name="conf" value="1" <?php echo $get_video_by_name['confidentialite']==1?"checked":"" ?>>PrivateLink </input>
                            <input type="radio" name="conf" value="2" <?php echo $get_video_by_name['confidentialite']==2?"checked":"" ?>>Private</input><br></p>
                            <p><button type="submit" class="btn btn-success pull-left">Modifier <i class="icon-white icon-ok-sign"></i></button>

                        </fieldset>
                    </form>

                </div>
            </div>
            </body>

        </div>

        <!-- <div class="span2">
            <?php
                //include_once 'vue/template/sidebar_droite.php';
            ?>
        </div> -->
    </div>
</div>

<footer>
    <?php
    include_once 'vue/template/footer.php';
    ?>
</footer>

</html>