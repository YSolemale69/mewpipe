<?php
if (isset($_SESSION['id']))
{
	header("location:/mewpipe/");
}
// session_destroy();
if (isset($_POST['nom'])) 
{
    include_once('modele/User.php');
    $strp = strlen($_POST['password']);

    //mot de passe différents
	if ($_POST['password'] != $_POST['confirmation']) 
	{
		$div_type = "error";
		$erreur_type = "Erreur !";
		$erreur = "Les mots de passe ne correspondent pas";
	}
	//mot de passe trop court
	elseif ($strp < 4) 
	{
		$div_type = "error";
		$erreur_type = "Erreur !";
		$erreur = "Votre mot de passe est trop court, minimum 6 caractères";
	}
	else
	{
        $user_count = User::get_user_by_email($_POST['email']);

        //email déjà utilisé
        if ($user_count > 0) 
        {
        	$div_type = "error";
			$erreur_type = "Erreur !";
			$erreur = "Cette adresse mail est déjà utilisée";
        }
        else
        {
            $user = User::set_user($_POST['nom'], $_POST['prenom'], $_POST['username'], $_POST['email'], $_POST['password']);

            /*$ds = ldap_connect("ADDS");  // assuming the LDAP server is on this host

			if ($ds) 
			{
			    // bind with appropriate dn to give update access
			    $r = ldap_bind($ds, "cn=administrateur, o=SUPINFO, c=FR", "Supinf0");

			    // prepare data
			    $info["cn"] = $_POST['nom']." ".$_POST['prenom'];
			    $info["sn"] = $_POST['username'];
			    $info["objectclass"] = "person";
			    $info["mail"] = $_POST['email'];

			    // add data to directory
			    $r = ldap_add($ds, "cn=".$_POST['nom']." ".$_POST['prenom'].", o=SUPINFO, c=FR", $info);

			    ldap_close($ds);
			} 
			else 
			{
			    echo "Unable to connect to LDAP server";
			}
			//end LDAP script
        	*/
        	$div_type = "success";
			$erreur_type = "Félicitation !";
			$erreur = "Votre compte a bien été créé, <a href='login'>Se connecter</a>";

            $user_count = User::get_user_by_email_password($_POST['email'], $_POST['password']);
            $_SESSION['id'] = $user_count['id'];
            $_SESSION['email'] = $user_count['email'];
            $_SESSION['username'] = $user_count['username'];
            $_SESSION['type'] = $user_count['type'];
            $_SESSION['register'] = true;
            header("location:/mewpipe/openid");
        }
	}
	include_once 'vue/auth/register.php';
}
else
{
	include_once 'vue/auth/register.php';
}