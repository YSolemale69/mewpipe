<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="icon" type="image/png" href="vue/css/img/favicon.ico" />
        <link rel="stylesheet" href="vue/css/style.css" />
        
        <link rel="stylesheet" href="../vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="../vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />

        <title>Profil</title>
</head>

<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span2">
            <?php
                //include_once 'vue/template/sidebar_gauche.php';
            ?>
        </div> -->

        <div class="span8 offset2">
            <?php
                include_once 'vue/template/header.php';
            ?>
        
            <body>
                <div class="row-fluid">
                    <div class="span12"> 
                        <h3>Profil de <?php echo $user['username'] ;?></h3>
                        <hr>

                        <h4>Mes informations personnelles</h4>
                        <hr>
                        <?php
                            echo "<br>";

                            echo $user['nom'];
                            echo "&nbsp";
                            echo $user['prenom'];

                            echo "<br>";
                            echo $user['email'];
                            echo "<br><br>";

                            
                            if ($user['type'] == 0) 
                            {
                                echo "compte administrateur";
                            }
                            elseif ($user['type'] == 1) 
                            {
                                echo "compte utilisateur";
                            }
                            else
                            {
                                echo "une erreur est survenue";
                            }

                        ?>

                        <?php
                            echo "<h4>Ses vidéos</h4>";
                      
                            foreach ($video as $v1 => $v2) 
                            {
                                echo "--- ";
                                echo "<a href='/mewpipe/watch/".$v2['nom']."'>".$v2['titre']."</a>";
                                echo "<br>";
                            }
                        ?>

                    </div>
                </div>
            </body>

        </div>

        <!-- <div class="span2">
            <?php
                //include_once 'vue/template/sidebar_droite.php';
            ?>
        </div> -->
    </div>
</div>

<footer>
    <?php
        include_once 'vue/template/footer.php';
    ?>
</footer>

</html>