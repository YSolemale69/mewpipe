<!DOCTYPE html>
<html>
    <head>
         <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="icon" type="image/png" href="/afort/vue/css/img/favicon.ico" />
        <link rel="stylesheet" href="/afort/vue/css/style.css" />
        
        <link rel="stylesheet" href="/afort/vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="/afort/vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />

		<title></title>
</head>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span2">
			<!-- SIDEBAR GAUCHE -->
			<?php
				include_once 'vue/template/sidebar_gauche.php';
			?>
		</div>

		<div class="span8">
			<!-- HEADER -->
			<?php
				include_once 'vue/template/header.php';
			?>
		
			<body>
				<div class="row-fluid">
					<div class="span12"> 
						<!-- CONTENU DE LA PAGE -->

						<!-- FIN DU CONTENU -->
					</div>
				</div>
			</body>

		</div>

		<div class="span2">
			<!-- SIDEBAR DROITE -->
			<?php
				include_once 'vue/template/sidebar_droite.php';
			?>
		</div>
	</div>
</div>

<footer>
	<!-- FOOTER -->
	<?php
		include_once 'vue/template/footer.php';
	?>
</footer>

</html>