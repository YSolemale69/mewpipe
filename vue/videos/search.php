<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/png" href="vue/css/img/favicon.ico" />
    <link rel="stylesheet" href="vue/css/style.css" />

    <link rel="stylesheet" href="vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />


    <title>Recherche</title>
</head>

<div class="container-fluid">
    <div class="row-fluid">
        <!-- <div class="span2">
			<?php
        //include_once 'vue/template/sidebar_gauche.php';
        ?>
		</div> -->

        <div class="span10 osffset2">
            <?php
            include_once 'vue/template/header.php';
            ?>

            <body>
            <div class="row-fluid">
                <div class="span12">
                    <h3>Resultat de la recherche</h3>
                    <?php
                    foreach ($video as $v1 => $v2)
                    {
                        echo "<div class = 'videolist'>
                                    <a href='/mewpipe/watch/{$v2['nom']}'><img height='110' width='196' src='thumbnails{$v2['thumbnail']}'/></a><br/>
                                    <a href='/mewpipe/watch/{$v2['nom']}'>{$v2['titre']}</a><br/>"
                            . $v2["vue"] . ($v2["vue"]<2?" vue":" vues")
                            ."</div>";
                    }
                    ?>

                </div>
            </div>
            </body>

        </div>

        <!-- <div class="span2">
			<?php
        //include_once 'vue/template/sidebar_droite.php';
        ?>
		</div> -->
    </div>
</div>

<footer>
    <?php
    include_once 'vue/template/footer.php';
    ?>
</footer>

</html>
