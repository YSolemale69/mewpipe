<?php
if(isset($_GET["state"]) && $_GET["state"] == $_SESSION["state"]){
    include_once('modele/User.php');
    $url = 'https://www.googleapis.com/oauth2/v4/token';
    $data = array(
        'code' => urlencode($_GET["code"]),
        'client_id' => urlencode("877013047826-nr9u91tmhe9ubsmtdefm2hglfkfa1c3v.apps.googleusercontent.com"),
        'client_secret' => urlencode("zYo5lk2K27zetOUvnUWNoZVe"),
        'redirect_uri' => urlencode("http://localhost/mewpipe/google"),
        'grant_type' => urlencode("authorization_code")
    );
    $data_string = "";
    //Change les données en url
    foreach($data as $key=>$value) { $data_string .= $key.'='.$value.'&'; }
    rtrim($data_string, '&');

    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $result = json_decode(curl_exec($ch));
    $accessToken = $result->access_token;
    $idToken = $result->id_token;
    $encodes = explode('.', $idToken);
    $json = json_decode(base64_decode($encodes[1]));
    curl_close($ch);

    $req = curl_init("https://www.googleapis.com/oauth2/v3/userinfo?access_token={$accessToken}");
    curl_setopt($req,CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 0);
    $userinfo = json_decode(curl_exec($req));
    var_dump($userinfo);
    curl_close($req);

    $user = User::get_user_by_google_id($userinfo->sub);
    if(isset($user["id"])){
        $_SESSION['id'] = $user['id'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['type'] = $user['type'];
        header("location:/mewpipe/profil");
    }else if(isset($_SESSION["id"])){
        User::update_user($_SESSION["id"],$userinfo->family_name,$userinfo->given_name,$userinfo->name,$userinfo->email,$userinfo->sub);
        header("location:/mewpipe/profil");
    }else{
        User::set_user($userinfo->family_name,$userinfo->given_name,$userinfo->name,$userinfo->email,null,$userinfo->sub);
        $user = User::get_user_by_google_id($userinfo->sub);
        $_SESSION['id'] = $user['id'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['type'] = $user['type'];
        header("location:/mewpipe/profil");
    }
}else{
    header("location:/mewpipe/login");
}