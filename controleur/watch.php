<?php
    //on récupère le "nom dans l'url de la video
	$query = isset($_GET["query"]) ? $_GET["query"] : "";
	$part = explode("/", $query);
	$video_nom = $part['1'];

if (isset($part['1']))    
{
	$video_nom = $part['1'];

    //on récupère les infos de la vidéo
    include_once('modele/Video.php');
	$get_video_by_name = Video::get_video_by_name($video_nom);
    $get_video_by_name?:header("location:/mewpipe/");

    //on récupère les catégories
    include_once('modele/Categorie.php');
    $categories = Categorie::get_categorie_by_video($get_video_by_name['id']);

    include_once('modele/Vues.php');

    ////ALGORITHME DE SUGGESTION////
    if (isset($_SESSION['id']))
    {
        //on récupère les 6 catégories les plus regardées par l'utilisateur
        $count_vue = Vues::get_view_by_user_id($_SESSION['id']);
        // var_dump($count_vue);

        $count_total_vues = Vues::get_view_by_user($_SESSION['id']);
        // echo "/count : ".$count_total_vues;

        foreach ($count_vue as $v1 => $v2) 
        {
            //on calcule le pourcentage
            $pourcentage = $v2['count']/$count_total_vues*100;
            //echo $pourcentage.'% - '; 
        }

    }



    ////VERIFICATION CONFIDENTIALITE////
    //si public
    if($get_video_by_name['confidentialite'] == 0)
    {
        Video::update_view($get_video_by_name['nom'], $get_video_by_name['vue']);
        $_SESSION['public_link_validity'] = 0;
        include_once 'vue/videos/watch.php';

        //ajout d'un enregistrement des catégories de la vidéo regardée
        if (isset($_SESSION['id']))
        {
            foreach ($categories as $v1 => $v2) 
            {
                $new_vue_type = Vues::set_view($_SESSION['id'], $v2["id_categorie"]);
            }
        }
    }
    //si private link et public link validity existe et = 255 sinon si private link et est le propriétaire
    elseif($get_video_by_name['confidentialite'] == 1 && (isset($_SESSION['public_link_validity']) && $_SESSION['public_link_validity']) == 255 || $get_video_by_name['user_id'] == $_SESSION['id'])
    {
        Video::update_view($get_video_by_name['nom'], $get_video_by_name['vue']);
        $_SESSION['public_link_validity'] = 0;
        include_once 'vue/videos/watch.php';

        //ajout d'un enregistrement des catégories regardée
        if (isset($_SESSION['id']))
        {
            foreach ($categories as $v1 => $v2) 
            {
                $new_vue_type = Vues::set_view($_SESSION['id'], $v2["id_categorie"]);
            }
        }
    }
    //si private et est membre ou propriétaire
    elseif($get_video_by_name['confidentialite'] == 2 && isset($_SESSION['id']) || $get_video_by_name['user_id'] == $_SESSION['id'])
    {
        Video::update_view($get_video_by_name['nom'],$get_video_by_name['vue']);
        include_once 'vue/videos/watch.php';

        //ajout d'un enregistrement des catégories regardée
        if (isset($_SESSION['id']))
        {
            foreach ($categories as $v1 => $v2) 
            {
                $new_vue_type = Vues::set_view($_SESSION['id'], $v2["id_categorie"]);
            }
        }
    }
    //si personnal est le propriétaire
    elseif ($get_video_by_name['confidentialite'] == 3 && $get_video_by_name['user_id'] == $_SESSION['id']) 
    {
        Video::update_view($get_video_by_name['nom'],$get_video_by_name['vue']);
        include_once 'vue/videos/watch.php';

        //ajout d'un enregistrement des catégories regardée
        if (isset($_SESSION['id']))
        {
            foreach ($categories as $v1 => $v2) 
            {
                $new_vue_type = Vues::set_view($_SESSION['id'], $v2["id_categorie"]);
            }
        }
    }
    else
    {
        header("location:/mewpipe/");
    }
}

else
{
    include_once 'vue/videos/watch.php';
}