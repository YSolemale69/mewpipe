<?php
if (!isset($_SESSION['id']))
{
	header("location:/mewpipe/");
}

include_once('modele/Categorie.php');
$categorie = Categorie::get_all_categories();

if (isset($_FILES['video']) && $_FILES['video']['name'] != '')    
{
	//ajout de video
	include_once 'controleur/ajout_video.php';
    $new_video = add_video('video');

    if (isset($new_video) && $new_video != '')
	{
		//on créé l'entrée de la vidéo dans la DB
		include_once('modele/Video.php');
    	$set_video = Video::set_video($_SESSION['id'], $new_video[0], $new_video[1], $GLOBALS['nom'], $_POST['titre'], $_POST['desc'], $_POST["conf"]);
		
		//on ajoute les catégories
		$video_id = Video::get_video_by_name($GLOBALS['nom']);
		foreach ($_POST['option'] as $v1 => $v2) 
        {
        	$new_video_categorie = Categorie::set_categorie($video_id['id'],$v2);
        	echo $new_video_categorie;
        }

		//si la confidentialité est sur "private link"
		if ($_POST["conf"] == 1) 
		{
			include_once('modele/Links.php');
			include_once('modele/Categorie.php');
			//on génère une url aléatoire
			$url = rand();

			//on récupère tout de suite l'id de la video
			$video_id = Video::get_video_by_name($GLOBALS['nom']);

			//on créé le lien
			$set_link = Links::set_link($video_id['id'], $GLOBALS['nom'], $url);
			$set_categorie = Categorie::set_categorie($video_id['id'], $_POST['categorie']);

			echo " url à copier: http://localhost/mewpipe/private_link/".$url;
		}
	}	
    	
	include_once 'vue/videos/video.php';
}

else
{
	include_once 'vue/videos/video.php';
}