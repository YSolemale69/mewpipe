<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="icon" type="image/png" href="/mewpipe/vue/css/img/favicon.ico" />
        <link rel="stylesheet" href="/mewpipe/vue/css/style.css" />
        
        <link rel="stylesheet" href="/mewpipe/vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="/mewpipe/vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />


		<title>Connexion</title>
</head>

<div class="container-fluid">

	<body>
		<div class="row-fluid">
			<div class="span3 offset5"> 

				<!-- formulaire de connexion  -->
				<form method="post" action="" id="formLogin">
				    <fieldset>
				        <legend>Connexion</legend>
				            <div class="control-group">
				                <div class="controls">

				                
				            	<!--   affichage des messages d'erreur  -->
				                <?php 
				                	$var=0;
				                	if (isset($erreur)) 
				                	{
				                		echo '<div class="alert alert-'.$div_type.'">
						                    <h4 class="alert-heading">'.$erreur_type.'</h4>
						                    '.$erreur.'</div>';
				                	}
				                ?>
				                 

				                <p><input type="email" name="email"  placeholder="Email" required><br></p>
				            
				                <p><input type="password" name="password"  placeholder="Mot de passe" required><br></p> 
				            

				                <p><button type="submit" class="btn btn-success pull-left">Se connecter <i class="icon-white icon-ok-sign"></i></button>
				                <a href="register">&nbsp Pas encore enregistré ?</a></p>
				                
				            	</div>
				        	</div>
				    </fieldset>
				</form>
                <a href="<?php echo $openid_url; ?>">Login with Google</a>
			</div>
		</div>
	</body>

</div>


</html>