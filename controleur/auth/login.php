<?php
if (isset($_SESSION['id']))
{
	header("location:/mewpipe/");
}
//set_include_path(get_include_path() . PATH_SEPARATOR . 'google-api-php-client/src');

if (isset($_POST['email'])) 
{
    include_once('modele/User.php');
    $user_count = User::get_user_by_email_password($_POST['email'], $_POST['password']);

    if (isset($user_count['id'])) 
    {
        $_SESSION['id'] = $user_count['id'];
        $_SESSION['email'] = $user_count['email'];
        $_SESSION['username'] = $user_count['username'];
        $_SESSION['type'] = $user_count['type'];
    	header("location:/mewpipe/profil");
    }
    else
    {

    	
    	$div_type = "error";
		$erreur_type = "Erreur !";
		$erreur = "Votre email ou votre mot de passe ne correspond pas";
		
    }
	
	include_once 'vue/auth/login.php';
}
else
{
    // Genere un token et le stock en session
    $state = md5(rand());
    $_SESSION["state"] = $state;
    $client_id = "877013047826-nr9u91tmhe9ubsmtdefm2hglfkfa1c3v.apps.googleusercontent.com";

    $openid_url = "https://accounts.google.com/o/oauth2/auth?client_id={$client_id}&response_type=code&scope=openid%20profile%20email&redirect_uri=http://localhost/mewpipe/google&state={$state}";

	include_once 'vue/auth/login.php';
}