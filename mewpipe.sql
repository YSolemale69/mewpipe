-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 26 Juin 2015 à 14:08
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `mewpipe`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `user_id`, `email`) VALUES
(1, 2, 'test@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(1, 'Actualités et politique'),
(2, 'Animation'),
(3, 'Animaux'),
(4, 'Auto/Moto'),
(5, 'Cinéma et divertissement'),
(6, 'Cuisine et santé'),
(7, 'Emissions tv'),
(8, 'Humour'),
(9, 'Jeux vidéo et autres'),
(10, 'Mode et beauté'),
(11, 'Musique'),
(12, 'Science et éducation'),
(13, 'Sport'),
(14, 'Technologie');

-- --------------------------------------------------------

--
-- Structure de la table `categories_relation`
--

CREATE TABLE IF NOT EXISTS `categories_relation` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_video` int(15) NOT NULL,
  `id_categorie` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_video` (`id_video`),
  KEY `id_categorie` (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `categories_relation`
--

INSERT INTO `categories_relation` (`id`, `id_video`, `id_categorie`) VALUES
(8, 51, 2),
(9, 51, 5),
(10, 52, 2),
(11, 52, 12),
(12, 52, 14),
(16, 54, 2),
(17, 54, 10),
(18, 54, 13),
(19, 55, 2),
(20, 55, 12),
(21, 55, 14),
(22, 56, 2),
(23, 56, 5),
(24, 57, 2),
(25, 57, 5),
(26, 58, 2),
(27, 58, 9),
(28, 58, 11);

-- --------------------------------------------------------

--
-- Structure de la table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `video_id` int(15) NOT NULL,
  `video_nom` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `video_id` (`video_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `links`
--

INSERT INTO `links` (`id`, `video_id`, `video_nom`, `url`, `date`) VALUES
(12, 51, '17960f37af6eb670251b6b24af914b24eeba07c216fe927878', '8743', '2015-06-24 19:38:39');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `google_id` varchar(50) DEFAULT NULL,
  `type` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `username`, `email`, `password`, `google_id`, `type`) VALUES
(1, 'test', 'test', 'test', 'test@test.com', 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4', NULL, 1),
(2, 'SolÃ©malÃ©', 'Yannis', 'kibi', 'test@gmail.com', 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4', NULL, 0),
(3, 'PIANELLI', 'Pascal', 'Shuzuru', 'pianelli.p@gmail.com', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', NULL, 0),
(8, 'test', 'test', 'test', 'test@test.fr', 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_views`
--

CREATE TABLE IF NOT EXISTS `user_views` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `categorie_id` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`categorie_id`),
  KEY `categorie_id` (`categorie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;

--
-- Contenu de la table `user_views`
--

INSERT INTO `user_views` (`id`, `user_id`, `categorie_id`) VALUES
(8, 1, 1),
(11, 1, 1),
(77, 1, 1),
(80, 1, 1),
(83, 1, 1),
(86, 1, 1),
(89, 1, 1),
(9, 1, 2),
(12, 1, 2),
(78, 1, 2),
(81, 1, 2),
(84, 1, 2),
(87, 1, 2),
(90, 1, 2),
(92, 1, 2),
(94, 1, 2),
(96, 1, 2),
(98, 1, 2),
(100, 1, 2),
(102, 1, 2),
(104, 1, 2),
(106, 1, 2),
(108, 1, 2),
(110, 1, 2),
(112, 1, 2),
(114, 1, 2),
(116, 1, 2),
(118, 1, 2),
(120, 1, 2),
(122, 1, 2),
(142, 1, 2),
(146, 1, 2),
(1, 1, 4),
(10, 1, 4),
(13, 1, 4),
(79, 1, 4),
(82, 1, 4),
(85, 1, 4),
(88, 1, 4),
(91, 1, 4),
(2, 1, 5),
(4, 1, 5),
(6, 1, 5),
(14, 1, 5),
(27, 1, 5),
(29, 1, 5),
(31, 1, 5),
(33, 1, 5),
(35, 1, 5),
(37, 1, 5),
(39, 1, 5),
(41, 1, 5),
(43, 1, 5),
(45, 1, 5),
(47, 1, 5),
(49, 1, 5),
(51, 1, 5),
(53, 1, 5),
(55, 1, 5),
(57, 1, 5),
(59, 1, 5),
(61, 1, 5),
(63, 1, 5),
(65, 1, 5),
(67, 1, 5),
(69, 1, 5),
(71, 1, 5),
(73, 1, 5),
(75, 1, 5),
(93, 1, 5),
(95, 1, 5),
(97, 1, 5),
(99, 1, 5),
(101, 1, 5),
(103, 1, 5),
(105, 1, 5),
(107, 1, 5),
(109, 1, 5),
(111, 1, 5),
(113, 1, 5),
(115, 1, 5),
(117, 1, 5),
(119, 1, 5),
(121, 1, 5),
(123, 1, 5),
(147, 1, 10),
(3, 1, 11),
(5, 1, 11),
(7, 1, 11),
(15, 1, 11),
(28, 1, 11),
(30, 1, 11),
(32, 1, 11),
(34, 1, 11),
(36, 1, 11),
(38, 1, 11),
(40, 1, 11),
(42, 1, 11),
(44, 1, 11),
(46, 1, 11),
(48, 1, 11),
(50, 1, 11),
(52, 1, 11),
(54, 1, 11),
(56, 1, 11),
(58, 1, 11),
(60, 1, 11),
(62, 1, 11),
(64, 1, 11),
(66, 1, 11),
(68, 1, 11),
(70, 1, 11),
(72, 1, 11),
(74, 1, 11),
(76, 1, 11),
(148, 1, 13),
(124, 2, 1),
(125, 2, 2),
(127, 2, 2),
(129, 2, 2),
(131, 2, 2),
(134, 2, 2),
(137, 2, 2),
(139, 2, 2),
(141, 2, 2),
(143, 2, 2),
(149, 2, 2),
(152, 2, 2),
(153, 2, 2),
(126, 2, 4),
(25, 2, 5),
(128, 2, 5),
(130, 2, 5),
(138, 2, 5),
(140, 2, 5),
(154, 2, 5),
(135, 2, 10),
(144, 2, 10),
(26, 2, 11),
(132, 2, 12),
(150, 2, 12),
(136, 2, 13),
(145, 2, 13),
(133, 2, 14),
(151, 2, 14);

-- --------------------------------------------------------

--
-- Structure de la table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `src` varchar(500) NOT NULL,
  `thumbnail` varchar(500) NOT NULL,
  `nom` varchar(500) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vue` int(11) NOT NULL,
  `confidentialite` int(11) NOT NULL,
  PRIMARY KEY (`id`,`nom`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Contenu de la table `video`
--

INSERT INTO `video` (`id`, `user_id`, `src`, `thumbnail`, `nom`, `titre`, `description`, `date`, `vue`, `confidentialite`) VALUES
(51, 2, '/user-2-kibi/17960f37af6eb670251b6b24af914b24eeba07c216fe927878.mp4', '/user-2-kibi/17960f37af6eb670251b6b24af914b24eeba07c216fe927878.png', '17960f37af6eb670251b6b24af914b24eeba07c216fe927878', 'naruto shippuden opening 16', '16eme opening, arc de la 4Ã¨me grande guerre ninja', '2015-06-24 19:38:39', 6, 1),
(52, 2, '/user-2-kibi/6816ed2452fd506de2ce022372d1ebdde11b19c0624716926.mp4', '/user-2-kibi/6816ed2452fd506de2ce022372d1ebdde11b19c0624716926.png', '6816ed2452fd506de2ce022372d1ebdde11b19c0624716926', 'test public', 'un test public', '2015-06-24 19:47:24', 7, 0),
(54, 2, '/user-2-kibi/251301ebb94def522c130477d40de9da0938520d27a547608.mp4', '/user-2-kibi/251301ebb94def522c130477d40de9da0938520d27a547608.png', '251301ebb94def522c130477d40de9da0938520d27a547608', 'test private', 'un test member only', '2015-06-24 19:49:10', 3, 2),
(55, 2, '/user-2-kibi/265134f4b0d209b73ef811896ec1933e1fa5fdc768fde19191.mp4', '/user-2-kibi/265134f4b0d209b73ef811896ec1933e1fa5fdc768fde19191.png', '265134f4b0d209b73ef811896ec1933e1fa5fdc768fde19191', 'test personnel', 'un test me only', '2015-06-24 19:49:55', 1, 3),
(56, 1, '/user-1-test/1893945115dad8f39ac3a7145e3a18bf92c26e914f46827761.mp4', '/user-1-test/1893945115dad8f39ac3a7145e3a18bf92c26e914f46827761.png', '1893945115dad8f39ac3a7145e3a18bf92c26e914f46827761', 'opening naruto 1', 'opening', '2015-06-24 19:54:02', 0, 0),
(57, 1, '/user-1-test/19030eb51281143293745291e3f8f9f1a11ebecee29a020420.mp4', '/user-1-test/19030eb51281143293745291e3f8f9f1a11ebecee29a020420.png', '19030eb51281143293745291e3f8f9f1a11ebecee29a020420', 'naruto ending 11', 'ending 11', '2015-06-24 19:54:38', 0, 0),
(58, 1, '/user-1-test/24237417a999b13b4f62f3e0ace7e81ba7048189237ab32115.mp4', '/user-1-test/24237417a999b13b4f62f3e0ace7e81ba7048189237ab32115.png', '24237417a999b13b4f62f3e0ace7e81ba7048189237ab32115', 'naruto opening 2', 'haruka kanata', '2015-06-24 19:55:16', 0, 0);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `categories_relation`
--
ALTER TABLE `categories_relation`
  ADD CONSTRAINT `categories_relation_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_relation_ibfk_2` FOREIGN KEY (`id_video`) REFERENCES `video` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `links`
--
ALTER TABLE `links`
  ADD CONSTRAINT `links_ibfk_1` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user_views`
--
ALTER TABLE `user_views`
  ADD CONSTRAINT `user_views_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_views_ibfk_2` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
