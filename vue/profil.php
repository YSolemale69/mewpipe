<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="icon" type="image/png" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/img/favicon.ico" />
        <link rel="stylesheet" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/style.css" />
        
        <link rel="stylesheet" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php if (isset($var_dir)) {echo $var_dir;} ?>vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />


		<title>Profil</title>
</head>

<div class="container-fluid">
	<div class="row-fluid">
		<!--<div class="span2">
			<?php
				//include_once 'vue/template/sidebar_gauche.php';
			?>
		</div>-->

		<div class="span8 offset2">
			<?php
				include_once 'vue/template/header.php';
			?>
		
			<body>
				<div class="row-fluid">
					<div class="span12"> 
						<h3>Mon profil</h3>
						<hr>

						<h4>Mes informations personnelles</h4>
						<hr>
						<?php
							//affichage des infos utilisateur
							echo "bonjour ".$user['username'];
							echo "<br>";

							echo $user['nom'];
							echo "&nbsp";
							echo $user['prenom'];

							echo "<br>";
							echo $user['email'];
							echo "<br><br>";

							//si admin
							if ($user['type'] == 0) 
							{
								echo "<div style='color:red'>compte administrateur</div>";
								echo "ATTENTION certaines de ces fonctionnalité peuvent piquer, faites gaffe!";
							}
							//si user lambda
							elseif ($user['type'] == 1) 
							{
								echo "compte utilisateur";
							}
							else
							{
								echo "une erreur est survenue";
							}

						?>

						<?php
							//affichage des vidéos perso
                            echo "<h4>Mes vidéos</h4>";
                            
                      
                            foreach ($video as $v1 => $v2) 
                            {
                            	//récupération du lien de partage
                            	$link = Links::get_links_by_video_nom($v2['nom']);

                            	//réucpération de la catégorie
                            	$categorie = Categorie::get_categorie_by_video($v2['id']);


                            	echo "--- ";
                                echo "<div class = 'videolist'>
                                    <a href='/mewpipe/watch/{$v2['nom']}'><img height='110' width='196' src='thumbnails{$v2['thumbnail']}'/></a><br/>
                                    <a href='/mewpipe/watch/{$v2['nom']}'>{$v2['titre']}</a><br/>"
                                    . $v2["vue"] . ($v2["vue"]<2?" vue":" vues")
                                    ."</div>";
                                echo "<a href='/mewpipe/editvideo/".$v2['nom']."'>Edit</a> ";
                                echo "<a href='/mewpipe/deletevideo/".$v2['nom']."'>Delete</a>";

                                //affichage du lien de partage
                                if (isset($link) && $link['url'] != '') 
                                {
                                	echo "<br>lien de partage: http://localhost/mewpipe/private_link/".$link['url'];
                                }
                            	echo "<br>";

                            	//afichage des catégories
                            	if (isset($categorie) && $categorie != '') 
                                {
                                	foreach ($categorie as $v1 => $v2) 
							        {
							        	echo utf8_encode($v2["nom"])."&nbsp|&nbsp";
							        }
                                    echo "<br>";
                                }
                            }
                        ?>

						<?php
						//affichage spécifique pour les admins
						if (isset($var) && $var ==1) 
						{
							echo '<div class="row-fluid">';
							echo '<div class="span4">';
							echo '<legend>Gestion des utilisateurs</legend>';

							foreach ($user_list as $key => $value) 
				            {
				            	echo $value['nom'];
								echo "&nbsp";
								echo $value['prenom'];
								echo "<br>";
				                echo $value['email'];
								echo "<br>";
								echo "<a href='/mewpipe/profil/delete_user/".$value['id']."'>Supprimer utilisateur</a>";
								echo "<br>";
								echo "<a href='/mewpipe/profil/edit_user/".$value['id']."'>Modifier utilisateur</a>";
								echo "<br>";
				                
				                echo "------------------";
								echo "<br>";
								
				            }
				            echo "</div>";

				            /////si ajout d'user/////
				            if ($form == 1) 
				            {
				        		echo '<div class="span4">';
			            		echo '
								<form method="post" action="" id="formAddUser">
								    <fieldset>
								        <legend>Ajout d\'utilisateur</legend>
								            <div class="control-group">
								                <div class="controls">';
			                	if (isset($erreur)) 
			                	{
			                		echo '<div class="alert alert-'.$div_type.'">
					                    <h4 class="alert-heading">'.$erreur_type.'</h4>
					                    '.$erreur.'</div>';
			                	}                

				                echo '
				
								                <p><input type="text" class="input-xlarge" name="nom"  placeholder="Nom" required><br></p>

								                <p><input type="text" class="input-xlarge" name="prenom"  placeholder="Prénom" required><br></p>

				   				                <p><input type="text" name="username" placeholder="Nom d\'utilisateur" required><br></p>

								                <p><input type="email" name="email"  placeholder="Email" required><br></p>
								            
								                <p><input type="password" name="password"  placeholder="Mot de passe" required><br></p> 
								            
								                <p><input type="password" name="confirmation"  placeholder="Confirmer le mot de passe" required><br></p>

								                <p><button type="submit" class="btn btn-success pull-left">Ajouter l\'utilisateur <i class="icon-white icon-ok-sign"></i></button>
								                
								            	</div>
								        	</div>
								    </fieldset>
								</form>
								';
								echo '</div>';
							}
							/////si modification des users////
							else
							{
								echo '<div class="span6">';
								echo '<form method="post" action="" id="formEditUser">
								    <fieldset>
								        <legend>modification d\'utilisateur</legend>
								            <div class="control-group">
								                <div class="controls">

								                <p><input type="text" class="input-xlarge" name="nom_edit" value="'. $edit_user["nom"].'" placeholder="'. $edit_user["nom"].'" required><br></p>

								                <p><input type="text" class="input-xlarge" name="prenom_edit" value="'. $edit_user["prenom"].'"  placeholder="'.$edit_user["prenom"].'" required><br></p>

				   				                <p><input type="text" name="username_edit" value="'. $edit_user["username"].'" placeholder="'.$edit_user["username"].'" required><br></p>

								                <p><input type="email" name="email_edit" value="'. $edit_user["email"].'" placeholder="'.$edit_user["email"].'" required><br></p>

								                <p><button type="submit" class="btn btn-success pull-left">Modifier les informations <i class="icon-white icon-ok-sign"></i></button>
								                
								            	</div>
								        	</div>
								    </fieldset>
								</form>';
								echo '</div>';
							}
							echo '</div>';
						}	
						?>

					</div>
				</div>
			</body>

		</div>

		<!--<div class="span2">
			<?php
				//include_once 'vue/template/sidebar_droite.php';
			?>
		</div>-->
	</div>
</div>

<footer>
	<?php
		include_once 'vue/template/footer.php';
	?>
</footer>

</html>