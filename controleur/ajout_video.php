<?php		
function add_video($video)
{	
	$GLOBALS['nom'] = '';
	$new_file = "error";

	$extensions_valides = array( 'avi' , 'mp4' , 'mpeg' , 'mkv', 'wmv', 'mov' );
	//1. strrchr renvoie l'extension avec le point (? . ?).
	//2. substr(chaine,1) ignore le premier caract?re de chaine.
	//3. strtolower met l'extension en minuscules.
	$extension_upload = strtolower(  substr(  strrchr($_FILES[$video]['name'], '.')  ,1)  );

	$videoname = strstr($_FILES[$video]['name'], '.', true);
	

	if ( in_array($extension_upload,$extensions_valides) )
	{
		$var_nom = rand().sha1(rand().$_FILES[$video]['name']).rand();
		$_FILES[$video]['name'] = $var_nom .".". $extension_upload; //genere un nom aleatoire 
	
		$dir_user = "/user-" . $_SESSION['id']."-".$_SESSION['username'];
		$dir = ROOT. "/mewpipe/videos" . $dir_user;
        $dir_thumbnail = ROOT. "/mewpipe/thumbnails" . $dir_user;

		if (!file_exists($dir)) 
		{
			mkdir($dir, 0777, true);
		}

        if (!file_exists($dir_thumbnail))
        {
            mkdir($dir_thumbnail, 0777, true);
        }
		
		$new_file = $dir . "/{$_FILES[$video]['name']}";
        $new_thumbnail = $dir_thumbnail . "/{$var_nom}.png";
        shell_exec("ffmpeg -i {$_FILES[$video]['tmp_name']} -vf thumbnail,scale=300:200 -frames:v 1 $new_thumbnail");
		$resultat = move_uploaded_file($_FILES[$video]['tmp_name'], $new_file);
	}

	$GLOBALS['nom'] = $var_nom;
	$new_video[0] = $dir_user . "/{$_FILES[$video]['name']}";
    $new_video[1] = $dir_user . "/{$var_nom}.png";
	return $new_video;
}