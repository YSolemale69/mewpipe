<?php
    // Genere un token et le stock en session
    $state = md5(rand());
    $_SESSION["state"] = $state;
    $client_id = "877013047826-nr9u91tmhe9ubsmtdefm2hglfkfa1c3v.apps.googleusercontent.com";

    $openid_url = "https://accounts.google.com/o/oauth2/auth?client_id={$client_id}&response_type=code&scope=openid%20profile%20email&redirect_uri=http://localhost/mewpipe/google&state={$state}";

    include_once 'vue/auth/openid.php';