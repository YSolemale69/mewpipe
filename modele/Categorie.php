<?php
class Categorie
{
	public static function set_categorie($id_video, $id_categorie)
    {
        global $bdd;

        $req = $bdd->prepare ('INSERT INTO categories_relation(id_video, id_categorie) VALUES(:id_video, :id_categorie)');
        $req->bindParam(':id_video', $id_video, PDO::PARAM_INT);
        $req->bindParam(':id_categorie', $id_categorie, PDO::PARAM_INT);
        $req->execute();
    }

    public static function get_all_categories()
    {
        global $bdd;

        $req = $bdd->prepare('SELECT * FROM categories');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_categorie_id_by_video_id($id_video)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT * FROM categories_relation WHERE id_video = :id_video');
        $req->bindParam(':id_video', $id_video, PDO::PARAM_INT);
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_video_id_by_categorie_id($id_categorie)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT * FROM categories_relation WHERE id_categorie = :id_categorie');
        $req->bindParam(':id_categorie', $id_categorie, PDO::PARAM_INT);
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_categorie_by_video($id_video)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT *
                                FROM categories
                                INNER JOIN categories_relation ON categories.id = categories_relation.id_categorie
                                WHERE categories_relation.id_video = :id_video');
        $req->bindParam(':id_video', $id_video, PDO::PARAM_INT);
        $req->execute();

        return $req->fetchAll();
    }
}