<?php
if(isset($_POST["search"])){
    include_once('modele/Video.php');
    $terms = explode(" ", $_POST["search"]);
    $video = Video::search_video($terms);

    include_once 'vue/videos/search.php';
}