<?php
class Vues
{
	public static function set_view($user_id, $categorie_id)
    {
        global $bdd;

        $req = $bdd->prepare ('INSERT INTO user_views(user_id, categorie_id) VALUES(:user_id, :categorie_id)');
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->bindParam(':categorie_id', $categorie_id, PDO::PARAM_INT);
        $req->execute();
    }

    public static function get_view_by_user_id($user_id)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT categorie_id, count(*) as "count" FROM user_views WHERE user_id = :user_id GROUP BY categorie_id ORDER BY count DESC LIMIT 5');
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_view_by_user($user_id)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT categorie_id FROM user_views WHERE user_id = :user_id');
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->execute();
        
        return $req->rowCount();
    }

    public static function get_categorie_count_by_user($user_id)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT *
                                FROM categories
                                INNER JOIN user_views ON categories.id = user_views.categorie_id
                                WHERE user_views.user_id = :user_id');
        $req->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $req->execute();

        return $req->fetchAll();
    }
}