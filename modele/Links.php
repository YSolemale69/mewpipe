<?php
class Links
{
	public static function set_link($video_id, $video_nom, $url)
    {
        global $bdd;

        $req = $bdd->prepare ('INSERT INTO links(video_id, video_nom, url) VALUES(:video_id, :video_nom, :url)');
        $req->bindParam(':video_id', $video_id, PDO::PARAM_INT);
        $req->bindParam(':video_nom', $video_nom, PDO::PARAM_STR, 500);
        $req->bindParam(':url', $url, PDO::PARAM_STR, 500);
        $req->execute();
    }

    public static function get_all_links()
    {
        global $bdd;

        $req = $bdd->prepare('SELECT * FROM links');
        $req->execute();

        return $req->fetchAll();
    }

    public static function get_links_by_url($url)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT * FROM links WHERE url = :url');
        $req->bindParam(':url', $url, PDO::PARAM_STR, 500);
        $req->execute();

        return $req->fetch();
    }

    public static function get_links_by_video_nom($video_nom)
    {
        global $bdd;

        $req = $bdd->prepare('SELECT * FROM links WHERE video_nom = :video_nom');
        $req->bindParam(':video_nom', $video_nom, PDO::PARAM_STR, 500);
        $req->execute();

        return $req->fetch();
    }
}