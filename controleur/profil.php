<?php
if (!isset($_SESSION['id']))
{
	header("location:/mewpipe/login");
}
else
{
    include_once('modele/User.php');
    $user = User::get_user_by_id($_SESSION['id']);

    include_once('modele/Video.php');
    $video = Video::get_video_by_user_id($_SESSION['id']);

    include_once('modele/Categorie.php');
    include_once('modele/Links.php');


    //vérification compte adminsitrateur
    if ($_SESSION['type'] == 0) 
    {
    	$admin = User::get_admin_by_id_email($_SESSION['id'], $_SESSION['email']);

    	if (isset($admin['id']))
    	{
            $form = 1;
                //on récupère les paramètres de l'url
                $query = isset($_GET["query"]) ? $_GET["query"] : "";
                $part = explode("/", $query);

                if (isset($part['2']) && $part['1'] == "delete_user") 
                {
                    $user_id = $part['2'];
                    $user_delete = User::delete_user($user_id);
                    header("location:/mewpipe/profil");
                    ////////suppression d'un utilisateur////////
                }
                elseif (isset($part['2']) && $part['1'] == "edit_user") 
                {
                    $var_dir = "../../";
                    //variable pour changer le répertoire du css
                    
                    $form = 2;
                    //variable pour changer le formulaire qui s'affiche

                    $user_id = $part['2'];
                    $edit_user = User::get_user_by_id($user_id);
                    ////////modification d'un utilisateur////////
                }


                

            ///////ajout utilisateur////////
            if (isset($_POST['nom'])) 
            {
                include_once('modele/User.php');
                $strp = strlen($_POST['password']);

                //mot de passe différents
                if ($_POST['password'] != $_POST['confirmation']) 
                {
                    $div_type = "error";
                    $erreur_type = "Erreur !";
                    $erreur = "Les mots de passe ne correspondent pas";
                }
                //mot de passe trop court
                elseif ($strp < 4) 
                {
                    $div_type = "error";
                    $erreur_type = "Erreur !";
                    $erreur = "Votre mot de passe est trop court, minimum 6 caractères";
                }
                else
                {
                    $user_count = User::get_user_by_email($_POST['email']);

                    //email déjà utilisé
                    if ($user_count > 0) 
                    {
                        $div_type = "error";
                        $erreur_type = "Erreur !";
                        $erreur = "Cette adresse mail est déjà utilisée";
                    }
                    else
                    {
                        $user = User::set_user($_POST['nom'], $_POST['prenom'], $_POST['username'], $_POST['email'], $_POST['password']);

                        
                        $div_type = "success";
                        $erreur_type = "Félicitation !";
                        $erreur = "Votre compte a bien été créé, <a href='login'>Se connecter</a>";
                    }
                }
            }
            ///////Fin ajout utilisateur/////////

            ///////modification utilisateur////////
            elseif (isset($_POST['nom_edit'])) 
            {
                include_once('modele/User.php');

                $update_user = User::update_user($user_id, $_POST['nom_edit'], $_POST['prenom_edit'], $_POST['username_edit'], $_POST['email_edit']);

                $div_type = "success";
                $erreur_type = "Félicitation !";
                $erreur = "Votre compte a bien été créé, <a href='login'>Se connecter</a>";
            
            }
            ///////Fin modification utilisateur/////////

            //affichage des utilisateurs
            $var = 1;
            $user_list = User::get_all_user();

            $array = array();
            foreach ($user_list as $key => $value) 
            {
                array_push($array, $value['email']);
            }
		}

    }
	include_once 'vue/profil.php';
} 