<?php
if (!isset($_SESSION['id']))
{
    header("location:/mewpipe/login");
}
$query = isset($_GET["query"]) ? $_GET["query"] : "";

$part = explode("/", $query);

if (isset($part['1']))
{
    $video_nom = $part['1'];

    include_once('modele/Video.php');
    include_once('controleur/suppr_video.php');
    $get_video_by_name = Video::get_video_by_name($video_nom);

    //si l'use est le propriétaire de la video on supprime la video et l'entrée dans la DB
    if($_SESSION['id'] == $get_video_by_name['user_id'])
    {
        Video::delete_video($get_video_by_name['id']);
        suppr_video($get_video_by_name['src']);
    }
}

header("location:/mewpipe/profil");