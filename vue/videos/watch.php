<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="icon" type="image/png" href="vue/css/img/favicon.ico" />
        <link rel="stylesheet" href="../vue/css/style.css" />
        
        <link rel="stylesheet" href="../vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="../vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />


		<title>Video</title>
</head>

<div class="container-fluid">
	<div class="row-fluid">
		<!-- <div class="span2">
			<?php
				//include_once 'vue/template/sidebar_gauche.php';
			?>
		</div> -->

		<div class="span8 offset2">
			<?php
				include_once 'vue/template/header.php';
			?>
		
			<body>
				<div class="row-fluid">
					<div class="span12"> 
						<h3> <?php echo $get_video_by_name["titre"];?></h3>
						<video width="640" height="480" controls>
 							<source src="<?php echo '../videos' . $get_video_by_name["src"];?>" type="video/mp4">
 							<source src="<?php echo '../videos' . $get_video_by_name["src"];?>" type="video/avi">
 							<source src="<?php echo '../videos' . $get_video_by_name["src"];?>" type="video/mpeg">
 							<source src="<?php echo '../videos' . $get_video_by_name["src"];?>" type="video/mkv">
 							<source src="<?php echo '../videos' . $get_video_by_name["src"];?>" type="video/wmv">

						Your browser does not support the video tag.
						</video>

					</div>
				</div>

				<div class="row-fluid">
					<div class="span4"> 
	                    <?php 
	                    	echo $get_video_by_name["vue"];
	                    	echo $get_video_by_name["vue"]<2?" vue":" vues";
	                    ?>
		            </div>

		            <div class="span6">
	                    <?php 
	                    	foreach ($categories as $v1 => $v2) 
					        {
					        	echo utf8_encode($v2["nom"])."&nbsp|&nbsp";
					        }
				        ?>
		            </div>
		        </div>

				<div class="row-fluid">
	                <div class="span8 offset2">
	                    <?php echo utf8_encode($get_video_by_name["description"]);?>
	                </div>
	            </div>
			</body>

		</div>

		<!-- <div class="span2">
			<?php
				//include_once 'vue/template/sidebar_droite.php';
			?>
		</div> -->
	</div>
</div>

<footer>
	<?php
		include_once 'vue/template/footer.php';
	?>
</footer>

</html>