<?php 
global $bdd;
try
{
    $bdd = new PDO('mysql:host=localhost;dbname=mewpipe', 'sysadmin', 'root');
    // On se connecte à MySQL
}

catch(Exception $e)
{
    die('Erreur : '.$e->getMessage());
    // En cas d'erreur, on affiche un message et on arrête tout
}