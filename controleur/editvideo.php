<?php
if (!isset($_SESSION['id']))
{
    header("location:/mewpipe/login");
}
$query = isset($_GET["query"]) ? $_GET["query"] : "";

$part = explode("/", $query);

if (isset($part['1']))
{
    $video_nom = $part['1'];

    include_once('modele/Video.php');
    $get_video_by_name = Video::get_video_by_name($video_nom);
    if (isset($_POST['titre']))
    {
        Video::update_video($get_video_by_name['id'], $_SESSION['id'], $_POST['titre'], $_POST['desc'], $_POST["conf"]);
        $get_video_by_name = Video::get_video_by_name($video_nom);
    }
    include_once 'vue/videos/edit_video.php';
}else{
    header("location:/mewpipe/profil");
}