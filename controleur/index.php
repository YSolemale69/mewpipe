<?php
	include_once('modele/connexion_sql.php');
	session_start();

	//on récupère le contenu de la requête
	$query = isset($_GET["query"]) ? $_GET["query"] : "";

	//on sépare les différentes parties de la requête pour récupérer des paramètres
    $part = explode("/", $query);


	//// GESTION DES ROUTES ////
	if($query != "index" && $query != "")
	{
		//si fichier lambda
		if (!empty($query) && is_file($query.'.php') )
		{
		    include_once $query.'.php';
		}
		//si le fichier est un controleur
		elseif (!empty($query) && is_file('controleur/'.$query.'.php') && $query != 'watch') 
		{
			include_once 'controleur/'.$query.'.php';
		}
		//si le fichier est dans /auth
		elseif (!empty($query) && is_file('controleur/auth/'.$query.'.php')) 
		{
			include_once 'controleur/auth/'.$query.'.php';
		}

		//pour les pages particulières qui prennent un paramètre
		elseif (!empty($query) && $part['0'] == 'watch' && isset($part['1'])) 
		{
			include_once 'controleur/watch.php';
		}
		elseif (!empty($query) && $part['0'] == 'user' && isset($part['1'])) 
		{
			include_once 'controleur/user.php';
		}
        elseif (!empty($query) && $part['0'] == 'editvideo' && isset($part['1']))
        {
            include_once 'controleur/editvideo.php';
        }
        elseif (!empty($query) && $part['0'] == 'deletevideo' && isset($part['1']))
        {
            include_once 'controleur/deletevideo.php';
        }
        elseif (!empty($query) && $part['0'] == 'profil' && isset($part['1']))
        {
            include_once 'controleur/profil.php';
        }
        //pour les private link
        elseif (!empty($query) && $part['0'] == 'private_link' && isset($part['1']))
        {
        	include_once('modele/Links.php');
        	$link = Links::get_links_by_url($part['1']);
        	if (isset($link) && $link['video_nom'] != '') 
        	{
        		session_start();
        		$_SESSION['public_link_validity'] = 255;
        		header("location:/mewpipe/watch/".$link['video_nom']);
        	}
            
        }
		else
		{
			header("location:/mewpipe/");
			include_once 'vue/index.php';
		}
	}
	elseif ($query == "vue") 
	{
			header("location:/mewpipe/");
			include_once 'vue/index.php';
	}
	else
	{
		include_once 'index.php';

		include_once('modele/Video.php');

		//affichage des dernières vidéos selon connecté ou pas
		if (isset($_SESSION['id']))
		{
			$popular_video = Video::get_popular_video();
        	$last_video = Video::get_last_video();
		}
		else
		{
			$popular_video = Video::get_popular_video_for_public();
        	$last_video = Video::get_last_video_for_public();
		}

    	

		include_once 'vue/index.php';
	}