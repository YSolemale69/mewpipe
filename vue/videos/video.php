<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="icon" type="image/png" href="vue/css/img/favicon.ico" />
        <link rel="stylesheet" href="vue/css/style.css" />
        
        <link rel="stylesheet" href="vue/css/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="vue/css/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />

        <script src="vue/css/bootstrap/js/bootstrap.min.js"></script>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript">
        	jQuery.fn.multiselect = function() {
			    $(this).each(function() {
			        var checkboxes = $(this).find("input:checkbox");
			        checkboxes.each(function() {
			            var checkbox = $(this);
			            // Highlight pre-selected checkboxes
			            if (checkbox.prop("checked"))
			                checkbox.parent().addClass("multiselect-on");
			 
			            // Highlight checkboxes that the user selects
			            checkbox.click(function() {
			                if (checkbox.prop("checked"))
			                    checkbox.parent().addClass("multiselect-on");
			                else
			                    checkbox.parent().removeClass("multiselect-on");
			            });
			        });
			    });
			};

			
        </script>

		<title>Video</title>
</head>

<div class="container-fluid">
	<div class="row-fluid">
		<!-- <div class="span2">
			<?php
				//include_once 'vue/template/sidebar_gauche.php';
			?>
		</div> -->

		<div class="span8 offset2">
			<?php
				include_once 'vue/template/header.php';
			?>
		
			<body>
				<div class="row-fluid">
					<div class="span12"> 
						<form method="post" action="" id="formVideo" enctype="multipart/form-data">
				    		<fieldset>
				        		<legend>Ajouter une vidéo</legend>

				                	<p><input type="text" class="input-xlarge" name="titre"  placeholder="Entrez un titre pour votre vidéo" required><br></p>
                                    <p><textarea class="input-xlarge" name="desc"  placeholder="Entrez une description pour votre vidéo"></textarea><br></p>
				        			Ajouter une video <input id="video" type="file" name="video"><br>
                                    <p></p><input type="radio" name="conf" value="0" checked>Public </input>
                                    <input type="radio" name="conf" value="1">PrivateLink </input>
                                    <input type="radio" name="conf" value="2">Private (member only)</input>
                                    <input type="radio" name="conf" value="3">Personnel (me only)</input><br></p>
                                    <p>
                                    	<div class="multiselect">
                                    		<?php 
                                    			foreach ($categorie as $v1 => $v2) 
					                            {
													echo "<label><input type='checkbox' name='option[]' value='".$v2['id']."' />".utf8_encode($v2['nom'])."</label>";
					                            }

                                    		?>
										</div>
                                    </p>
				                	<p><button type="submit" class="btn btn-success pull-left">Envoyer <i class="icon-white icon-ok-sign"></i></button>

					        </fieldset>
						</form>

					</div>
				</div>
			</body>

		</div>

		<!-- <div class="span2">
			<?php
				//include_once 'vue/template/sidebar_droite.php';
			?>
		</div> -->
	</div>
</div>

<footer>
	<?php
		include_once 'vue/template/footer.php';
	?>
</footer>

</html>